
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var IDJI = [];
var visinaITM=-1;
var težaITM=-1;
var TIM = 500;
var k1 = 0;
var k2 = 0;
/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {

         var ehrId;
        var ime;
        var priimek;
        var datumRojstva;
        
   document.getElementById("preberiPredlogoBolnika").disabled = false;
   document.getElementById("preberiObstojeciEHR").disabled = false;
   document.getElementById("BranjeMeritev").disabled = false;
   document.getElementById("VnosMeritev").disabled = false;

if(stPacienta==1){
     ime = "Janez";
     priimek = "Benti";
     datumRojstva = "1957-03-10"+"T00:00:00.000Z";
} 

if(stPacienta==2){
     ime = "Ana";
     priimek = "Sir";
     datumRojstva = "1989-06-05"+"T00:00:00.000Z";
}  
      
if(stPacienta==3){
     ime = "Peter";
     priimek = "Maze";
     datumRojstva = "1994-02-09"+"T00:00:00.000Z";
}   


          	$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        ehrId = data.ehrId;
        IDJI[stPacienta-1] = ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: {"ehrId": ehrId}
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              $("#kreirajSporociloNav").html("<span class='obvestilo " +
                "label label-success fade-in'>Podatki uspešno generirani.</span>");
                Dopolni(stPacienta);
            }
          },
          error: function(err) {
          	$("#kreirajSporociloNav").html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
          }
        });
      }
});
 
          }
        

// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

function Dopolni(stPacienta){
            if(stPacienta==1){
              zapisiMeritve(IDJI[0], "177.4", "114.9", "2009-05-20T09:30", "36.1", "95" );
              zapisiMeritve(IDJI[0], "177.6", "122.0", "2011-05-20T09:30", "36.0", "96" );
              zapisiMeritve(IDJI[0], "178.5", "119.3", "2013-05-20T09:30", "35.9", "93" );
              zapisiMeritve(IDJI[0], "176.4", "113.2", "2014-05-20T09:30", "35.8", "91" );
              zapisiMeritve(IDJI[0], "178.4", "134.7", "2015-05-20T09:30", "36.2", "92" );
              zapisiMeritve(IDJI[0], "178.3", "123.5", "2016-05-20T09:30", "35.7", "94" );
            }

            else if(stPacienta==2){
              zapisiMeritve(IDJI[1], "165.5", "81.2", "2008-05-20T09:30", "37.3", "90" );
              zapisiMeritve(IDJI[1], "165.5", "83.3", "2010-05-20T09:30", "35.5", "89" );
              zapisiMeritve(IDJI[1], "165.4", "84.5", "2012-05-20T09:30", "35.8", "96" );
              zapisiMeritve(IDJI[1], "165.4", "86.2", "2013-04-20T09:30", "36.0", "91" );
              zapisiMeritve(IDJI[1], "165.5", "88.7", "2015-05-20T09:30", "35.7", "92" );
              zapisiMeritve(IDJI[1], "165.4", "90.2", "2016-05-20T09:30", "36.2", "93" );
            }

            else if(stPacienta==3){
              zapisiMeritve(IDJI[2], "185.4", "63.2", "2014-05-20T09:30", "35.8", "98" );
              zapisiMeritve(IDJI[2], "186.5", "64.5", "2015-02-20T09:30", "35.9", "97" );
              zapisiMeritve(IDJI[2], "187.4", "65.0", "2015-08-20T09:30", "36.5", "99" );
              zapisiMeritve(IDJI[2], "188.3", "67.9", "2016-02-20T09:30", "35.2", "97" );
              zapisiMeritve(IDJI[2], "189.4", "69.5", "2017-02-20T09:30", "36.4", "96" );
              zapisiMeritve(IDJI[2], "190.7", "70.5", "2017-03-20T08:30", "36.1", "99" );
            } 
}

 function	zapisiMeritve(ID,visina,teza,cas,temperatura,nasicenost){
   //console.log(ID);
   	if (!ID || ID.trim().length == 0) {
	} else {
		var vnos = {
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": cas,
		    "vital_signs/height_length/any_event/body_height_length": visina,
		    "vital_signs/body_weight/any_event/body_weight": teza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": temperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenost
		};
		var parametriZahteve = {
		    ehrId: ID,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: 'unknown'
		};
		$.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(vnos),
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (res) {
        //console.log("sssss");
      },
      error: function(err) {
                //console.log("sssss");
      }
		});
	}
 }

  function dodajPodatke(){
    	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
var datumRojstva = $("#kreirajDatumRojstva").val();
var ehrIdznan;

    if(document.getElementById("kreirajIme").value.length!=0 && document.getElementById("kreirajPriimek").value.length!=0 && document.getElementById("kreirajDatumRojstva").value.length!=0 && document.getElementById("kreirajEHR").value.length!=0){
          	$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrIdznan = document.getElementById("kreirajEHR").value;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: {"ehrId": ehrIdznan}
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              $("#kreirajSporocilo").html("<span class='obvestilo " +
                "label label-success fade-in'>Bolnik uspešno dodan.</span>");
              $("#preberiEHRid").val(ehrIdznan);
            }
          },
          error: function(err) {
          	$("#kreirajSporocilo").html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
          }
        });
      }
});
      
    }
    
    else if(document.getElementById("kreirajIme").value.length!=0 && document.getElementById("kreirajPriimek").value.length!=0 && document.getElementById("kreirajDatumRojstva").value.length!=0 && document.getElementById("kreirajEHR").value.length==0){
                	$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: {"ehrId": ehrId}
        };
        document.getElementById("PreberiEHR").value=ehrId;
        document.getElementById("PreberiEHRvnos").value=ehrId;
        document.getElementById("PreberiMeritev").value=ehrId;
        
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              $("#kreirajSporocilo").html("<span class='obvestilo " +
                "label label-success fade-in'>Bolnik uspešno dodan.</span>");
              $("#kreirajEHR").val(ehrId);
            }
          },
          error: function(err) {
          	$("#kreirajSporocilo").html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
          }
        });
      }
});
    }
    
    else{
      		$("#kreirajSporocilo").html("<span class='obvestilo label " + "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
    }
  }

 function PreberiEHR(){
    var EHR = document.getElementById("PreberiEHR").value;
    console.log(EHR);
    var party;
    				$.ajax({
			url: baseUrl + "/demographics/ehr/" + EHR + "/party",
			type: 'GET',
			headers: {
        "Authorization": getAuthorization()
      },
    	success: function (data) {
  			party = data.party;
    $("#kreirajSporociloEHR").html("<span class='obvestilo label label-success fade-in'> EHR ID pripada bolniku: " +party.firstNames+" "+party.lastNames+", rojenemu "+party.dateOfBirth+"");
          console.log(party.dateOfBirth);
  		},

  		
		});
    
 }

 function	VnesiMeritve(){
   var ID = document.getElementById("PreberiEHRvnos").value;
   var cas = document.getElementById("Cas-meritve").value;
   var visina = document.getElementById("Visina").value;
   var teza = document.getElementById("Teza").value;
   var temperatura = document.getElementById("Temperatura").value;
   var nasicenost = document.getElementById("Nasicenost").value;
   console.log("seos");
   	if (!ID || ID.trim().length == 0) {
	$("#SporociloEHRvnos").html("<span class='obvestilo label " + "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
      console.log("seos");
	} else {
		var vnos = {
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": cas,
		    "vital_signs/height_length/any_event/body_height_length": visina,
		    "vital_signs/body_weight/any_event/body_weight": teza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": temperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenost
		};
		var parametriZahteve = {
		    ehrId: ID,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: 'unknown'
		};
		$.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(vnos),
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (res) {
        $("#SporociloEHRvnos").html(
          "<span class='obvestilo label label-success fade-in'>" +
          "Vnos uspešen" + ".</span>");
      },
      error: function(err) {
      	$("#SporociloEHRvnos").html(
          "<span class='obvestilo label label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
      }
		});
	}
 }
 
 function PreberiMeritve(){


   var IDD = document.getElementById("PreberiMeritev").value;
   //console.log(IDD);
   document.getElementById("rezultatMeritveVitalnihZnakov").innerHTML = "";
   if(document.getElementById("visinacheck").checked == true){
       					$.ajax({
    				  url: baseUrl + "/view/" + IDD + "/" + "height",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			    	if (res.length > 0) {
    			    	  var prejsnjiCAS = "1911-05-20T09:30";
    				    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna višina</th></tr>";
  				        for (var i in res) {
  				          //console.log(prejsnjiCAS);
  				          //console.log(res[i].time);
  				          if(res[i].time.localeCompare(prejsnjiCAS)==0){
  				            //console.log("seos");
  				          }
  				          else{
    		            results += "<tr><td>" + res[i].time +
                      "</td><td class='text-right'>" + res[i].height +
                      " " + res[i].unit + "</td></tr>";
  				        }
  				        //console.log(res[i].time.localeCompare(prejsnjiCAS));
  				        prejsnjiCAS = res[i].time;
  				        //console.log(prejsnjiCAS);
  				        //console.log(i);
  				        visinaITM = res[i].height;
  				        }
  				        //console.log(res);
  				        results += "</table>";
  				        $("#rezultatMeritveVitalnihZnakov").append(results);
  				         $("#kreirajSporociloBranje").html(
                    "<span class='obvestilo label label-success fade-in'>" +
                    "Meritve uspešno prebrane.</span>");
    			    	} else {
    			    		$("#kreirajSporociloBranje").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
    			    	}
    			    },
    			    error: function() {
    			    	$("#kreirajSporociloBranje").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
    			    }
});
   }
   else if(document.getElementById("tezacheck").checked == true){
       					$.ajax({
    				  url: baseUrl + "/view/" + IDD + "/" + "weight",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			    	if (res.length > 0) {
    			    	  var prejsnjiCAS = "1911-05-20T09:30";
    				    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna teža</th></tr>";
  				        for (var i in res) {
  				          //console.log(prejsnjiCAS);
  				          //console.log(res[i].time);
  				          if(res[i].time.localeCompare(prejsnjiCAS)==0){
  				            //console.log("seos");
  				          }
  				          else{
    		            results += "<tr><td>" + res[i].time +
                      "</td><td class='text-right'>" + res[i].weight +
                      " " + res[i].unit + "</td></tr>";
  				        }
  				        //console.log(res[i].time.localeCompare(prejsnjiCAS));
  				        prejsnjiCAS = res[i].time;
  				        //console.log(prejsnjiCAS);
  				        //console.log(i);
  				        težaITM = res[i].weight;
  				        }
  				        //console.log(res);
  				        results += "</table>";
  				        $("#rezultatMeritveVitalnihZnakov").append(results);
  				         $("#kreirajSporociloBranje").html(
                    "<span class='obvestilo label label-success fade-in'>" +
                    "Meritve uspešno prebrane.</span>");
    			    	} else {
    			    		$("#kreirajSporociloBranje").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
    			    	}
    			    },
    			    error: function() {
    			    	$("#kreirajSporociloBranje").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
    			    }
});
   }
   else if(document.getElementById("temperaturacheck").checked == true){
       					$.ajax({
    				  url: baseUrl + "/view/" + IDD + "/" + "body_temperature",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
              },
    			    success: function (res) {
    			    	if (res.length > 0) {
    			    	  var prejsnjiCAS = "1911-05-20T09:30";
    				    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna temperatura</th></tr>";
  				        for (var i in res) {
  				          //console.log(prejsnjiCAS);
  				          //console.log(res[i].time);
  				          if(res[i].time.localeCompare(prejsnjiCAS)==0){
  				            //console.log("seos");
  				          }
  				          else{
    		            results += "<tr><td>" + res[i].time +
                      "</td><td class='text-right'>" + res[i].temperature +
                      " " + res[i].unit + "</td></tr>";
  				        }
  				        //console.log(res[i].time.localeCompare(prejsnjiCAS));
  				        prejsnjiCAS = res[i].time;
  				        //console.log(prejsnjiCAS);
  				        //console.log(i);
  				        }
  				        //console.log(res);
  				        results += "</table>";
  				        $("#rezultatMeritveVitalnihZnakov").append(results);
  				        $("#kreirajSporociloBranje").html(
                    "<span class='obvestilo label label-success fade-in'>" +
                    "Meritve uspešno prebrane.</span>");
    			    	} else {
    			    		$("#kreirajSporociloBranje").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
    			    	}
    			    },
    			    error: function() {
    			    	$("#kreirajSporociloBranje").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
    			    }
});
   }
      document.getElementById("rezultatMeritveVitalnihZnakov").classList.add("rob");
      if(visinaITM != -1 && težaITM != -1){
      ITM(visinaITM,težaITM);
}

 }
 function ITM(visina,teza){
   var tm = teza / ((visina/100)*(visina/100));
   var predlog;
   if(tm<=20){
     predlog = 600;
   }
   else if(tm>20 && tm<=25){
     predlog = 490;
   }
   else if(tm>25){
     predlog = 380;
   }
   document.getElementById("kalorije").innerHTML = predlog;
   TIM = predlog;
 }

function move(kal) {
  var elem = document.getElementById("myBar");
  var width = (kal/TIM)*100;
  elem.style.width = width+'%';
  document.getElementById("PROCENTI").innerHTML = width;

} 

function sestej(){
  document.getElementById("skupno").innerHTML = k1+k2;
  if(k1 + k2 > TIM){
    document.getElementById("sporocilo").innerHTML = "Vaš obrok presega priporočeno vrednost kalorij";
  }
    else{
    document.getElementById("sporocilo").innerHTML = "Vaš obrok ustreza priporočeni vrednosti kalorij";
  }
}
 
$(document).ready(function() {


  /**
   * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju
   * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
   * padajočega menuja (npr. Pujsa Pepa).
   */
   document.getElementById("preberiPredlogoBolnika").disabled = true;
   document.getElementById("preberiObstojeciEHR").disabled = true;
   document.getElementById("BranjeMeritev").disabled = true;
   document.getElementById("VnosMeritev").disabled = true;
   
  $('#preberiPredlogoBolnika').change(function() {
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajDatumRojstva").val(podatki[2]);
    $("#kreirajEHR").val("");
});

  $('#preberiObstojeciEHR').change(function() {
    var podatki2 = $(this).val();
    $("#PreberiEHR").val(IDJI[podatki2[0]]);

});
  $('#BranjeMeritev').change(function() {
    var podatki4 = $(this).val().split(",");
    $("#PreberiMeritev").val(IDJI[podatki4[0]]);

});

  $('#VnosMeritev').change(function() {
    var podatki3 = $(this).val().split(",");
    $("#PreberiEHRvnos").val(IDJI[podatki3[0]]);
    $("#Cas-meritve").val(podatki3[1]);
    $("#Visina").val(podatki3[2]);
    $("#Teza").val(podatki3[3]);
    $("#Temperatura").val(podatki3[4]);
    $("#Nasicenost").val(podatki3[5]);

});
  $('#Sendvic').change(function() {
    var kal;
    var mast;
    var OH;
    var prot;
    var sol;
    var kruh = $(this).val();
    var ime;
    console.log(kruh);
    $.getJSON('https://world.openfoodfacts.org/api/v0/product/'+kruh+'.json', function(data) {
      console.log(kruh);
      if(kruh==3242272875055){
        kal = data.product.nutriments.energy / 3;
        prot = data.product.nutriments.proteins;
        console.log("seos");
        ime = " s šunko"
      }
        else if(kruh==2000000025906){
        kal = data.product.nutriments.energy_value;
        prot = 12; 
        ime = " s salamo"
      }
      else if(kruh==9011600004658){
        kal = data.product.nutriments.energy_value;
        prot = data.product.nutriments.proteins;
        ime = " s tuno"
      }
      mast = data.product.nutriments.fat;
      OH = data.product.nutriments.carbohydrates_value;
      sol = data.product.nutriments.salt;
      console.log(data.product.nutriments);
      document.getElementById("kalorije2").innerHTML = kal;
      document.getElementById("fat").innerHTML = mast;
      document.getElementById("carbs").innerHTML = OH;
      document.getElementById("proteins").innerHTML = prot;
      document.getElementById("salt").innerHTML = sol;
      k1 = kal;
      move(kal);
      document.getElementById("IzbranSendvic").innerHTML = ime;
      sestej();
    });
  });

  $('#Pijaca').change(function() {
    var kal;
    var mast;
    var OH;
    var prot;
    var sol;
    var kruh = $(this).val();
    var ime;
    console.log(kruh);
    $.getJSON('https://world.openfoodfacts.org/api/v0/product/'+kruh+'.json', function(data) {
      console.log(kruh);
      if(kruh==8024884117809){
        prot = data.product.nutriments.proteins;

        ime = "Voda"
      }
        else if(kruh==3292090141221){
 
        ime = "Coca Cola"
      }
      else if(kruh==4890008200511){

        ime = "Pomarančni sok"
      }
      kal = data.product.nutriments.energy_value;
      prot = data.product.nutriments.proteins;
      mast = data.product.nutriments.fat;
      OH = data.product.nutriments.carbohydrates_value;
      sol = data.product.nutriments.salt;
      console.log(data.product.nutriments);
      document.getElementById("kalorije2").innerHTML = kal;
      document.getElementById("fat").innerHTML = mast;
      document.getElementById("carbs").innerHTML = OH;
      document.getElementById("proteins").innerHTML = prot;
      document.getElementById("salt").innerHTML = sol;
      k2 = kal;
      move(kal);
      document.getElementById("IzbranaPijaca").innerHTML = ime;
      sestej();
    });
  });
//zadnji ready zaklepaj
});